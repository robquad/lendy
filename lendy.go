package lendy

import (
	"fmt"
)

type Invester interface {
	Invest() int
	fmt.Stringer
}

func Debug(s interface{}) {
	fmt.Printf("Debug: %s\n", s)
}
