# Lendy

Before running lendy:
* rename config.default.json to config.json and fill in the values
* change filters as desired at the bottom of lc.go
* customize run times in main.go

After building you can run lendy without any options and it will execute at the scheduled times. Alternatively you can use the -once flag to buy notes immediately and exit.

The default filters are only for example and should not be used as is.

### Use at your own risk
