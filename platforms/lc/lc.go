package lc

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"os"
	"sort"
	"time"
)

func init() {
	f, fe := os.OpenFile("./errors.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if fe != nil {
		log.Fatal(fe)
	}

	log.SetOutput(f)
}

func handleError(err error) {
	if err != nil {
		fmt.Println(err)
		log.Fatal(err)
	}
}

func (lc *LendingClub) makeRequest(method string, url string, input interface{}, output interface{}) int {
	client := &http.Client{}

	im, err := json.Marshal(input)
	req, err := http.NewRequest(method, url, bytes.NewBuffer(im))
	handleError(err)

	req.Header.Add("Authorization", lc.Key)
	if input != nil {
		req.Header.Add("Content-type", "application/json")
	}
	reqD, _ := httputil.DumpRequestOut(req, true)
	resp, err := client.Do(req)
	handleError(err)

	if resp.StatusCode < 200 || resp.StatusCode > 300 {
		respD, _ := httputil.DumpResponse(resp, true)
		log.Printf("Request:\n %s\n\nResponse:\n %s\n\n", reqD, respD)
		fmt.Printf("Request:\n %s\n\nResponse:\n %s\n\n", reqD, respD)
	}

	defer resp.Body.Close()
	content, err := ioutil.ReadAll(resp.Body)
	handleError(err)

	err = json.Unmarshal(content, output)
	handleError(err)

	return resp.StatusCode
}

func (lc *LendingClub) Invest() {
	if !lc.CanInvest() {
		fmt.Println("Insufficient funds")
		return
	}

	s := time.Now()
	var listedAt time.Time

	fmt.Printf("Start investing at %v\n", s)
	oldLoans := make(map[int]bool)
	newLoans := 0
	newLoanFails := 0
	newLoanSuccesses := 0

	for newLoanFails < 12 {
		loans := lc.GetAvailableLoans(false)
		if len(loans) == 0 || time.Since(loans[0].ListD).Hours() > 1 {
			newLoanFails++
			time.Sleep(time.Second * 5)
			continue
		}

		goodLoans, nl := loans.filter(lc, oldLoans)
		if nl > 0 {
			if newLoanSuccesses == 0 {
				listedAt = time.Now()
			}
			newLoanFails = 0
			newLoanSuccesses++
			newLoans += nl
			lc.BuyNotes(goodLoans)
		} else {
			newLoanFails++
			time.Sleep(time.Second * 5)
		}
	}

	fmt.Printf("It took %v for %d new loans to be listed. Loans were retrieved in %d request.\n", listedAt.Sub(s), newLoans, newLoanSuccesses)
}

func (lc *LendingClub) InvestOnce() {
	if !lc.CanInvest() {
		fmt.Println("Insufficient funds")
		return
	}

	s := time.Now()
	loans := lc.GetAvailableLoans(true)
	goodLoans, _ := loans.filter(lc, make(map[int]bool))
	lc.BuyNotes(goodLoans)
	fmt.Printf("It took %v to get and filter %d loans.  %d were good.\n", time.Now().Sub(s), len(loans), len(goodLoans))
}

func (lc *LendingClub) CanInvest() bool {
	return lc.Account.AvailableCash >= 25.0
}

func (lc *LendingClub) GetAvailableLoans(showAll bool) Loans {
	loans := ListedLoans{}
	url := "https://api.lendingclub.com/api/investor/v1/loans/listing"
	if showAll {
		url += "?showAll=true"
	}
	lc.makeRequest("GET", url, nil, &loans)
	return loans.Loans
}

func (lc *LendingClub) BuyNotes(loans Loans) {
	investAmount := 25.0
	if lc.Account.AvailableCash > 50.0*5 {
		investAmount = 50.0
	}
	limit := int(lc.Account.AvailableCash / investAmount)
	if limit > 5 {
		limit = 5
	}
	if len(loans) < 1 {
		fmt.Print("No loans to invest in.\n")
		return
	} else if len(loans) > limit {
		loans = loans[:limit]
	}

	lm := make(map[int]Loan)

	o := OrderRequest{}
	o.Aid = lc.InvestorId
	o.Orders = make([]Order, 0, 1)
	for _, l := range loans {
		lm[l.ID] = l
		o.Orders = append(o.Orders, Order{l.ID, investAmount, lc.PortfolioID})
	}

	logEntry := fmt.Sprintf("Attempted to purchase %d loans at %v\n", len(loans), time.Now())
	r := OrderResponse{}
	url := fmt.Sprintf("https://api.lendingclub.com/api/investor/v1/accounts/%d/orders", lc.InvestorId)
	lc.makeRequest("POST", url, o, &r)
	fmt.Printf("Notes purchased at %v\n", time.Now())

	for _, oc := range r.OrderConfirmations {
		l := lm[oc.LoanID]
		j, _ := json.MarshalIndent(l, "", " ")
		logEntry += fmt.Sprintf("Attempted to buy:\n %s\n%s\n\n\n", j, oc.ExecutionStatus)
		success := false
		for _, es := range oc.ExecutionStatus {
			if es == "ORDER_FULFILLED" {
				fmt.Printf("Purchased %d Grade: %s Int Rate: %.2f FICO Low: %d\n", l.ID, l.SubGrade, l.IntRate, *(l.FicoRangeLow))
				lc.Account.AvailableCash -= oc.InvestedAmount
				success = true
			}
		}

		if !success {
			fmt.Printf("Failed to purchase %d Grade: %s Int Rate: %.2f FICO Low: %d Funded Amount: %.2f\n", l.ID, l.SubGrade, l.IntRate, *(l.FicoRangeLow), l.FundedAmount)
		}
	}

	f, err := os.OpenFile("./buyLoans.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	handleError(err)
	defer f.Close()

	f.WriteString(logEntry)
	f.Sync()
}

func isTooRecent(t time.Time, months int64) bool {
	return t.IsZero() || time.Since(t).Hours() < float64(months*30*24) //rough estimated
}

func (loans Loans) filter(lc *LendingClub, ol map[int]bool) (Loans, int) {
	g := make(Loans, 0, 5)
	nl := 0
	for _, l := range loans {
		if ol[l.ID] || lc.OwnedMap[l.ID] {
			continue
		} else {
			ol[l.ID] = true
			nl++
		}

		switch {
		case l.Grade != "B":
			continue
		case l.InitialListStatus == "W":
			continue
		case l.AnnualInc != nil && l.Installment > (*(l.AnnualInc)/12)*0.1:
			continue
		case l.AnnualIncJoint != nil && l.Installment > (*(l.AnnualIncJoint)/12)*0.1:
			continue
		case l.IsIncVerified == "NOT_VERIFIED" || l.IsIncVJoint == "NOT_VERIFIED":
			continue
		case l.Purpose == "small_business":
			continue
		case l.AccNowDelinq != nil && *(l.AccNowDelinq) > 0:
			continue
		case l.EmpLength == nil || *(l.EmpLength) < 18:
			continue
		case l.Dti != nil && *(l.Dti) > 30.0 && l.Purpose != "debt_consolidation" && l.Purpose != "credit_card":
			continue
		case l.Delinq2Yrs != nil && *(l.Delinq2Yrs) > 0:
			continue
		case isTooRecent(l.EarliestCrLine, 60):
			continue
		case l.FicoRangeLow == nil || *(l.FicoRangeLow) < 700:
			continue
		case l.MthsSinceLastDelinq != nil:
			continue
		case l.MthsSinceLastRecord != nil:
			continue
		case l.MthsSinceRecentRevolDelinq != nil:
			continue
		case l.MortAcc != nil && *(l.MortAcc) > 1:
			continue
		case l.PubRec != nil && *(l.PubRec) > 0:
			continue
		case l.MthsSinceRecentBcDlq != nil && *(l.MthsSinceRecentBcDlq) < 7*12:
			continue
		case l.PubRecBankruptcies != nil && *(l.PubRecBankruptcies) > 0:
			continue
		case l.NumAcctsEver120Ppd != nil && *(l.NumAcctsEver120Ppd) > 0:
			continue
		case l.ChargeoffWithin12Mths != nil && *(l.ChargeoffWithin12Mths) > 0:
			continue
		case l.Collections12MthsExMed != nil && *(l.Collections12MthsExMed) > 0:
			continue
		case l.TaxLiens != nil && *(l.TaxLiens) > 0:
			continue
		case l.MthsSinceLastMajorDerog != nil && *(l.MthsSinceLastMajorDerog) < 7*12:
			continue
		case l.NumTl90GDpd24M != nil && *(l.NumTl90GDpd24M) > 0:
			continue
		case l.NumTl30Dpd != nil && *(l.NumTl30Dpd) > 0:
			continue
		case l.NumTl120Dpd2M != nil && *(l.NumTl120Dpd2M) > 0:
			continue
		case l.TotCollAmt != nil && *(l.TotCollAmt) > 0:
			continue
		case l.DtiJoint != nil && *(l.DtiJoint) > 30.0 && l.Purpose != "debt_consolidation" && l.Purpose != "credit_card":
			continue
		case l.InqLast6Mths != nil && *(l.InqLast6Mths) > 1:
			continue
		}

		g = append(g, l)
	}

	//fmt.Printf(" %d loans available.  %d are left after filtering\n", len(loans), len(g))

	sort.Sort(g)

	f, err := os.Create("./filteredLoans.debug")
	handleError(err)
	defer f.Close()

	j, err := json.MarshalIndent(g, "", " ")
	f.Write(j)
	f.Sync()

	return g, nl
}
