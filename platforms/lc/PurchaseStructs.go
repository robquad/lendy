package lc

type Order struct {
	LoanID          int     `json:"loanId"`
	RequestedAmount float64 `json:"requestedAmount"`
	PortfolioID     int     `json:"portfolioId"`
}

type OrderRequest struct {
	Aid    int     `json:"aid"`
	Orders []Order `json:"orders"`
}

type OrderConfirmation struct {
	LoanID          int      `json:"loanId"`
	RequestedAmount float64  `json:"requestedAmount"`
	InvestedAmount  float64  `json:"investedAmount"`
	ExecutionStatus []string `json:"executionStatus"`
}

type OrderResponse struct {
	OrderInstructID    int                 `json:"orderInstructId"`
	OrderConfirmations []OrderConfirmation `json:"orderConfirmations"`
}
