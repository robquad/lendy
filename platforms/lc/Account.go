package lc

import (
	"fmt"
)

type LendingClub struct {
	Key         string
	InvestorId  int
	PortfolioID int
	Account     *AccountSummary
	Notes       *NotesOwned
	OwnedMap    map[int]bool
}

type AccountSummary struct {
	InvestorID           int
	AvailableCash        float64
	AccountTotal         float64
	AccruedInterest      float64
	InfundingBalance     float64
	ReceivedInterest     float64
	ReceivedPrincipal    float64
	ReceivedLateFees     float64
	OutstandingPrincipal float64
	TotalNotes           int
	TotalPortfolios      int
}

func (lc *LendingClub) String() string {
	return fmt.Sprintf("Platform: Lending Club | Investor Id: %d\n Available cash: $%.2f  Total: $%.2f  Notes: %d\n",
		lc.InvestorId, lc.Account.AvailableCash, lc.Account.AccountTotal, lc.Account.TotalNotes)
}

func (lc *LendingClub) UpdateAccount() {
	url := fmt.Sprintf("https://api.lendingclub.com/api/investor/v1/accounts/%d/summary", lc.InvestorId)

	lc.makeRequest("GET", url, nil, &(lc.Account))

	url = fmt.Sprintf("https://api.lendingclub.com/api/investor/v1/accounts/%d/notes", lc.InvestorId)
	lc.makeRequest("GET", url, nil, &(lc.Notes))

	lc.OwnedMap = make(map[int]bool)
	for _, n := range lc.Notes.MyNotes {
		lc.OwnedMap[n.LoanID] = true
	}

	fmt.Print(lc)
}

func New(key string, id int, portfolio int) LendingClub {
	lc := LendingClub{}
	lc.Key = key
	lc.InvestorId = id
	lc.PortfolioID = portfolio
	lc.UpdateAccount()

	return lc
}
