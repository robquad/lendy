package lc

import (
	"time"
)

type Loan struct {
	ID                         int       `json:"id"`
	MemberID                   int       `json:"memberId"`
	LoanAmount                 float64   `json:"loanAmount"`
	FundedAmount               float64   `json:"fundedAmount"`
	Term                       int       `json:"term"`
	IntRate                    float64   `json:"intRate"`
	ExpDefaultRate             float64   `json:"expDefaultRate"`
	ServiceFeeRate             float64   `json:"serviceFeeRate"`
	Installment                float64   `json:"installment"`
	Grade                      string    `json:"grade"`
	SubGrade                   string    `json:"subGrade"`
	EmpLength                  *int      `json:"empLength"`
	HomeOwnership              string    `json:"homeOwnership"`
	AnnualInc                  *float64  `json:"annualInc"`
	IsIncVerified              string    `json:"isIncV"`
	AcceptD                    string    `json:"acceptD"`
	ExpD                       string    `json:"expD"`
	ListD                      time.Time `json:"listD"`
	CreditPullD                string    `json:"creditPullD"`
	ReviewStatusD              string    `json:"reviewStatusD"`
	ReviewStatus               string    `json:"reviewStatus"`
	Desc                       string    `json:"desc"`
	Purpose                    string    `json:"purpose"`
	AddrZip                    string    `json:"addrZip"`
	AddrState                  string    `json:"addrState"`
	InvestorCount              *int      `json:"investorCount"`
	IlsExpD                    time.Time `json:"ilsExpD"`
	InitialListStatus          string    `json:"initialListStatus"`
	EmpTitle                   string    `json:"empTitle"`
	AccNowDelinq               *int      `json:"accNowDelinq"`
	AccOpenPast24Mths          *int      `json:"accOpenPast24Mths"`
	BcOpenToBuy                *int      `json:"bcOpenToBuy"`
	PercentBcGt75              *float64  `json:"percentBcGt75"`
	BcUtil                     *float64  `json:"bcUtil"`
	Dti                        *float64  `json:"dti"`
	Delinq2Yrs                 *int      `json:"delinq2Yrs"`
	DelinqAmnt                 *float64  `json:"delinqAmnt"`
	EarliestCrLine             time.Time `json:"earliestCrLine"`
	FicoRangeLow               *int      `json:"ficoRangeLow"`
	FicoRangeHigh              *int      `json:"ficoRangeHigh"`
	InqLast6Mths               *int      `json:"inqLast6Mths"`
	MthsSinceLastDelinq        *int      `json:"mthsSinceLastDelinq"`
	MthsSinceLastRecord        *int      `json:"mthsSinceLastRecord"`
	MthsSinceRecentInq         *int      `json:"mthsSinceRecentInq"`
	MthsSinceRecentRevolDelinq *int      `json:"mthsSinceRecentRevolDelinq"`
	MthsSinceRecentBc          *int      `json:"mthsSinceRecentBc"`
	MortAcc                    *int      `json:"mortAcc"`
	OpenAcc                    *int      `json:"openAcc"`
	PubRec                     *int      `json:"pubRec"`
	TotalBalExMort             *int      `json:"totalBalExMort"`
	RevolBal                   *float64  `json:"revolBal"`
	RevolUtil                  *float64  `json:"revolUtil"`
	TotalBcLimit               *int      `json:"totalBcLimit"`
	TotalAcc                   *int      `json:"totalAcc"`
	TotalIlHighCreditLimit     *int      `json:"totalIlHighCreditLimit"`
	NumRevAccts                *int      `json:"numRevAccts"`
	MthsSinceRecentBcDlq       *int      `json:"mthsSinceRecentBcDlq"`
	PubRecBankruptcies         *int      `json:"pubRecBankruptcies"`
	NumAcctsEver120Ppd         *int      `json:"numAcctsEver120Ppd"`
	ChargeoffWithin12Mths      *int      `json:"chargeoffWithin12Mths"`
	Collections12MthsExMed     *int      `json:"collections12MthsExMed"`
	TaxLiens                   *int      `json:"taxLiens"`
	MthsSinceLastMajorDerog    *int      `json:"mthsSinceLastMajorDerog"`
	NumSats                    *int      `json:"numSats"`
	NumTlOpPast12M             *int      `json:"numTlOpPast12m"`
	MoSinRcntTl                *int      `json:"moSinRcntTl"`
	TotHiCredLim               *int      `json:"totHiCredLim"`
	TotCurBal                  *int      `json:"totCurBal"`
	AvgCurBal                  *int      `json:"avgCurBal"`
	NumBcTl                    *int      `json:"numBcTl"`
	NumActvBcTl                *int      `json:"numActvBcTl"`
	NumBcSats                  *int      `json:"numBcSats"`
	PctTlNvrDlq                *int      `json:"pctTlNvrDlq"`
	NumTl90GDpd24M             *int      `json:"numTl90gDpd24m"`
	NumTl30Dpd                 *int      `json:"numTl30dpd"`
	NumTl120Dpd2M              *int      `json:"numTl120dpd2m"`
	NumIlTl                    *int      `json:"numIlTl"`
	MoSinOldIlAcct             *int      `json:"moSinOldIlAcct"`
	NumActvRevTl               *int      `json:"numActvRevTl"`
	MoSinOldRevTlOp            *int      `json:"moSinOldRevTlOp"`
	MoSinRcntRevTlOp           *int      `json:"moSinRcntRevTlOp"`
	TotalRevHiLim              *int      `json:"totalRevHiLim"`
	NumRevTlBalGt0             *int      `json:"numRevTlBalGt0"`
	NumOpRevTl                 *int      `json:"numOpRevTl"`
	TotCollAmt                 *int      `json:"totCollAmt"`
	ApplicationType            string    `json:"applicationType"`
	AnnualIncJoint             *float64  `json:"annualIncJoint"`
	DtiJoint                   *float64  `json:"dtiJoint"`
	IsIncVJoint                string    `json:"isIncVJoint"`
	OpenAcc6M                  *int      `json:"openAcc6m"`
	OpenIl6M                   *int      `json:"openIl6m"`
	OpenIl12M                  *int      `json:"openIl12m"`
	OpenIl24M                  *int      `json:"openIl24m"`
	MthsSinceRcntIl            *int      `json:"mthsSinceRcntIl"`
	TotalBalIl                 *float64  `json:"totalBalIl"`
	ILUtil                     *float64  `json:"iLUtil"`
	OpenRv12M                  *int      `json:"openRv12m"`
	OpenRv24M                  *int      `json:"openRv24m"`
	MaxBalBc                   *float64  `json:"maxBalBc"`
	AllUtil                    *float64  `json:"allUtil"`
	InqFi                      *int      `json:"inqFi"`
	TotalCuTl                  *int      `json:"totalCuTl"`
	InqLast12M                 *int      `json:"inqLast12m"`
}

type Loans []Loan

type ListedLoans struct {
	AsOfDate time.Time `json:"asOfDate"`
	Loans    Loans     `json:"loans"`
}

func (slice Loans) Len() int {
	return len(slice)
}

func (slice Loans) Less(i, j int) bool {
	return slice[i].IntRate > slice[j].IntRate
}

func (slice Loans) Swap(i, j int) {
	slice[i], slice[j] = slice[j], slice[i]
}
