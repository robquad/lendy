package lc

type NotesOwned struct {
	MyNotes []struct {
		LoanID           int     `json:"loanId"`
		NoteID           int     `json:"noteId"`
		OrderID          int     `json:"orderId"`
		InterestRate     float64 `json:"interestRate"`
		LoanLength       int     `json:"loanLength"`
		LoanStatus       string  `json:"loanStatus"`
		Grade            string  `json:"grade"`
		LoanAmount       int     `json:"loanAmount"`
		NoteAmount       int     `json:"noteAmount"`
		PaymentsReceived float64 `json:"paymentsReceived"`
		IssueDate        string  `json:"issueDate"`
		OrderDate        string  `json:"orderDate"`
		LoanStatusDate   string  `json:"loanStatusDate"`
	} `json:"myNotes"`
}
