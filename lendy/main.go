package main

import (
	"flag"
	"fmt"

	"github.com/robfig/cron"
	"github.com/spf13/viper"
	"gitlab.com/robquad/lendy/platforms/lc"
)

func main() {
	runOnce := flag.Bool("once", false, "invests in all available good loans and exits")
	flag.Parse()

	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		fmt.Printf("Config error: %s", err)
		return
	}

	var a = lc.New(viper.GetString("Key"), viper.GetInt("InvestorId"), viper.GetInt("PortfolioId"))

	if *runOnce {
		a.InvestOnce()
	} else {
		c := cron.New()

		c.AddFunc("0 55 7 * * *", a.UpdateAccount)
		c.AddFunc("0 0 8 * * *", a.Invest)

		c.AddFunc("0 55 11 * * *", a.UpdateAccount)
		c.AddFunc("0 0 12 * * *", a.Invest)

		c.AddFunc("0 55 15 * * *", a.UpdateAccount)
		c.AddFunc("0 0 16 * * *", a.Invest)

		c.AddFunc("0 55 19 * * *", a.UpdateAccount)
		c.AddFunc("0 0 20 * * *", a.Invest)

		c.Start()

		<-(chan int)(nil)
	}
}
